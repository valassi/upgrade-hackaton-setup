#!/bin/bash

if [ "$1" == "+g" ]; then
  projs="Gaudi"
  shift
elif [ "$1" == "-g" ]; then
  projs="LHCb Lbcom Rec Brunel"
  shift
else
  projs="Gaudi LHCb Lbcom Rec Brunel"
fi

if [ $# -lt 1 ]; then
  echo "Usage: $0 [-g|+g] command [arguments]"
  echo "  -g  Exclude Gaudi"
  echo "  +g  Only Gaudi"
  exit 1
fi

# See https://stackoverflow.com/a/17094284/1831046 about "$@" vs "$*"
for proj in $projs; do
  cd $proj
  echo "=== $(pwd)"
  "$@"
  echo
  cd ..
done
