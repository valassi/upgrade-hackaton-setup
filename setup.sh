#!/bin/bash
export CCACHE_DIR=${PWD}/../.ccache
#export CCACHE_LOGFILE=$(dirname $(mktemp -u))/ccache.debug
export CCACHE_CPP=2
export CMAKEFLAGS="-DCMAKE_USE_CCACHE=ON -DLOKI_BUILD_FUNCTOR_CACHE=FALSE"
export CMAKE_PREFIX_PATH=${PWD}:${CMAKE_PREFIX_PATH}
export VERBOSE=
export PATH=${PATH}:/cvmfs/lhcb.cern.ch/lib/contrib/ninja/1.4.0/x86_64-slc6

# Workaround for LBCORE-1591 (CMake 3.11.0 uses the wrong ninja)
# [NB: CMake 3.3.2 is not ok: subcommand FILTER needs at least CMake 3.6]
export PATH=/cvmfs/lhcb.cern.ch/lib/contrib/CMake/3.9.2/Linux-x86_64/bin:${PATH}
# This is a better workaround, but fails for Geant4 that does not support ninja
###export CMAKEFLAGS="${CMAKEFLAGS} -DCMAKE_MAKE_PROGRAM:FILEPATH=/cvmfs/lhcb.cern.ch/lib/contrib/ninja/1.4.0/x86_64-slc6/ninja"
# NB: Both workarounds above will become unnecessary when Gaudi !695 is merged
# [Using 3.11 may also speed up Geant4 builds, which are ninja-less and slow]

# Tweaks for LXPLUS
if [[ $(hostname) == lxplus* ]]; then
  # use the faster TMPDIR
  export CCACHE_TEMPDIR=$(dirname $(mktemp -u))/ccache
  mkdir -p ${CCACHE_TEMPDIR}
  # don't be too aggressive or else g++ gets killed
  export NINJAFLAGS=-j6
fi

# Tweaks for GEANT4 (using Make, no ninja due to Fortran) on PMPE04
# [This is useless: most Geant4 Makefile's contain the .NOTPARALLEL. target]
###if [[ $(hostname) == pmpe04 ]]; then
###  export MAKEFLAGS=-j16
###fi

# Hack to produce more verbose builds
# See https://groups.google.com/forum/#!topic/ninja-build/efudxpWrlTc
# [NB: move this to Makefile because it breaks bash PS1 prompts]
###export TERM=dumb
