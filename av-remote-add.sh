#!/bin/bash

# Gaudi
cd Gaudi
if ! git remote | grep -q upstream; then git remote add upstream https://:@gitlab.cern.ch:8443/gaudi/Gaudi.git; fi
###git fetch upstream        # FETCHES TOO MUCH
###git fetch upstream master # NO LONGER WORKS?
git fetch upstream master:refs/remotes/upstream/master
if ! git remote | grep -q lbfork; then git remote add lbfork https://:@gitlab.cern.ch:8443/lhcb/Gaudi.git; fi
###git fetch lbfork          # FETCHES TOO MUCH
cd ..

# Other projects
for proj in LHCb Lbcom Rec Brunel; do
  cd $proj
  if ! git remote | grep -q upstream; then git remote add upstream https://:@gitlab.cern.ch:8443/lhcb/$proj.git; fi
  ###git fetch upstream        # FETCHES TOO MUCH
  ###git fetch upstream master # NO LONGER WORKS?
  ###git fetch upstream TDR    # NO LONGER WORKS?
  git fetch upstream master:refs/remotes/upstream/master
  git fetch upstream TDR:refs/remotes/upstream/TDR
cd ..
done
