#!/usr/bin/env python
import os
import sys
import pwd
import grp
import platform
import optparse
import socket
import threading
from subprocess import Popen, call

version = 1
DEFAULT_TAG = '20170119'  # must be kept in sync with lb-docker-build

class autofs(threading.Thread):
    def __init__(self, dirs):
        threading.Thread.__init__(self, name="t1docker")
        self.dirs = dirs
        self.is_stop = threading.Event()
    def run(self):
        while not self.is_stop.is_set():
            self.is_stop.wait(120)
            for d in self.dirs:
                os.stat(d)
    def stop(self):
        self.is_stop.set()
        self.join()

def stop_threads():
    for i in threading.enumerate():
        if i.name == "t1docker":
            if i.is_alive():
                i.stop()

cwd = os.getcwd()
cwd_stat = os.stat(cwd)
userId = cwd_stat.st_uid
userName = pwd.getpwuid(cwd_stat.st_uid).pw_name
grpId = cwd_stat.st_gid
grpName = grp.getgrgid(cwd_stat.st_gid).gr_name
dockerHome = '/home/' + userName

if dockerHome in cwd:
    dockerHome = '/' + userName

if platform.system() == "Linux":
    if grpId is not 0:
        try:
            docker_group = grp.getgrnam('docker').gr_gid
        except:
            sys.exit('No "Docker" group found')
    else:
        if userName not in grp.getgrnam('docker').gr_mem:
            sys.exit('Directory owned by root, you need to be part of the "Docker" group to run a container')

parser = optparse.OptionParser()
parser.add_option('--version', action='store_true', dest='version', help="Print version information and quit")
parser.add_option('--os', action='store', help="OS you want to run",
                    choices=['slc5', 'slc6', 'centos7'])
parser.add_option('--centos7', action='store_const', const='centos7', dest='os', help="Run an centos7 container")
parser.add_option('--slc5', action='store_const', const='slc5', dest='os', help="Run an slc5 container")
parser.add_option('--slc6', action='store_const', const='slc6', dest='os', help="Run an slc6 container, default one")

parser.add_option('-c', action='store', dest='cmd', help="Run CMD when container is started")
parser.add_option('-e', '--env', action='append', dest='envVar', help="Set environment variables")
parser.add_option('-p', '--publish', action='append', dest='port', help="Publish a container's port(s) to the host")
parser.add_option('-u', '--user', action='store', dest='user', help="Username or UID (format: <name|uid>[:<group|gid>])")
parser.add_option('-v', '--volume', action='append', dest='volume', help="Bind mount a volume")
parser.add_option('-w', '--workdir', action='store', dest='workdir', help="Working directory inside the container")

parser.add_option('--docker-hostname', action='store_true', dest='hostname', help="Pass to the container the host's hostname")
parser.add_option('--dns', action='store', dest='dns', help="Set custom DNS server")
parser.add_option('--force-cvmfs', action='store_true', dest='forceCvmfs', help="Mount cvmfs from the container if not mounted on the host")
parser.add_option('--home', action='store_true', dest='home', help="Set a persistent home for this image")
parser.add_option('--kerberos', action='store_true', dest='kerberos', help="Forward the kerberos ticket to the container")
parser.add_option('--no-cvmfs', action='store_true', dest='noCvmfs', help="Do not mount cvmfs")
parser.add_option('--no-interactive', action='store_false', dest='interactive', help="Do not give an interactive shell to this container")
parser.add_option('--no-lb-su', action='store_const', const='no', dest='lb_su', help="Do not switch user")
parser.add_option('--no-LbLogin', action='store_const', const='no', dest='LbLogin', help="Do not call LbLogin on start")
parser.add_option('--no-network-settings', action='store_true', dest='networkSettings', help="Use the default container network settings instead of the host network stack")
parser.add_option('--privileged', action='store_true', dest='privileged', help="Give extended privileges to this container")
parser.add_option('--ssh-agent', action='store_true', dest='sshAgent', help="Forward host ssh-agent to the container")
parser.add_option('--update', action='store_true', dest='update', help="Update the docker image if a newer is available")
parser.add_option('--use-absolute-path', action='store_true', dest='absolutePath', help="Mount the host current directory with the same absolute path")

parser.set_defaults(version=False,
                    os='slc6',
                    LbLogin='yes',
                    lb_su='yes',
                    networkSettings=False,
                    home=False,
                    privileged=False,
                    interactive=True,
                    sshAgent=False,
                    hostname=False,
                    kerberos=False,
                    absolutePath=False,
                    graphicTools=False,
                    noCvmfs=False,
                    forceCvmfs=False,
                    update=False,
                    envVar=[],
                    port=[],
                    volume=[])

options, args = parser.parse_args()

if options.version == True:
    print("lb-docker-run version : " + str(version) + '\nTag for docker images : ' + str(DEFAULT_TAG))
    sys.exit()

if call(['docker', '--version']) is not 0:
    sys.exit('Please, be sure you have correctly installed Docker')

image_name = 'lhcbdev/{0}-build:{1}'.format(options.os, DEFAULT_TAG)

default_cvmfs_volumes = ['/cvmfs/lhcb.cern.ch', '/cvmfs/lhcbdev.cern.ch', '/cvmfs/sft.cern.ch']

need_autofs_thread = False
if options.noCvmfs == False:
    if options.forceCvmfs:
        if options.os == 'slc5':
            parser.error('cvmfs mounting is not supported on SLC5')
        image_name = 'lhcbdev/{0}-build-cvmfs:{1}'.format(options.os, DEFAULT_TAG)
        options.envVar.extend(['dockerMountCVMFS=yes'])
    elif not all(os.path.exists(p) and os.listdir(p) for p in default_cvmfs_volumes):
        sys.exit("Please, be sure you have cvmfs mounted on the host machine, or look at the options")
    else:
        options.volume.extend(default_cvmfs_volumes)
        need_autofs_thread = True
else:
    options.LbLogin='no'

if options.cmd and args:
    parser.error('you cannot -c in conjunction with arguments')

if not args:
    args.append('/bin/bash')

cmd = ['docker', 'run', '--rm',
        '-e', 'dockerLb=' + options.LbLogin + ':' + options.lb_su]

if options.absolutePath:
    options.volume.extend([cwd])
    if not options.workdir:
        options.workdir = cwd
else:
    options.volume.extend([cwd + ':/workspace'])

if options.home:
    homedir = os.getenv('HOME') + '/.local/docker/' + options.os + '-build-home/'
    if not os.path.exists(homedir):
        os.makedirs(homedir)
    options.volume.extend([homedir + ':' + dockerHome])
    options.envVar.extend(['dockerHome=' + dockerHome])

if options.interactive:
    cmd.extend(['-it'])

if options.privileged or options.forceCvmfs:
    if not options.privileged:
        print('WARNING: --force-cvmfs implies --privileged')
    cmd.extend(['--privileged'])

if options.sshAgent:
    options.volume.extend(['{0}:/ssh-agent'.format(os.getenv('SSH_AUTH_SOCK'))])
    options.envVar.extend(['SSH_AUTH_SOCK=/ssh-agent'])

if options.hostname:
    options.envVar.extend(['docker_hostname=' + socket.gethostname()])

if options.kerberos:
    options.envVar.extend(['KRB5CCNAME'])
    options.volume.extend([os.getenv('KRB5CCNAME').split(':')[1]])

if options.user:
    cmd.extend(['-u', options.user])
else:
    options.envVar.extend(['WORK_USER=' + userName + ':' + grpName])
    options.envVar.extend(['WORK_USER_ID={0}:{1}'.format(userId, grpId)])

for env in options.envVar:
    cmd.extend(['-e', env])

for vol in options.volume:
    if vol.find(':') >= 0:
        cmd.extend(['-v', vol])
    else:
        cmd.extend(['-v', vol + ':' + vol])

for port in options.port:
    cmd.extend(['-p', port])

if options.workdir:
    cmd.extend(['-w', options.workdir])

if options.networkSettings == False and options.dns:
    sys.exit("you can not set a dns with --net option enabled, look at the options")
elif options.networkSettings == False:
    cmd.append('--net=host')
elif options.dns:
    cmd.extend(['--dns', options.dns])

cmd.extend([image_name])

if options.cmd:
    cmd.extend(['/bin/lb-su', options.cmd])
else:
    cmd.extend(args)

if options.update == True:
    call(['docker', 'pull', image_name])

try:
    if need_autofs_thread:
        t1docker = autofs(dirs=[v for v in options.volume
                                if v.startswith('/cvmfs/')])
        t1docker.start()
    sys.exit(Popen(cmd).wait())
finally:
    stop_threads()
